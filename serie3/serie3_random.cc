#include "main.h"

// -------- Les variables qui définissent le problème
bool Unif, Norm, Pois;
size_t nsize = 1;
double mean = 0, sdev = 1;
std::string filename("output.txt");
// ------- Le générateur de nombres aléatoires et la "seed"
long int seed = 0;
std::mt19937 rng;
// ------- output stream
std::ofstream outf;

/* ------- Fonction auxiliaire qui traite les arguments du programme
           et assigne les valeurs aux variables correspondantes
 */
void parametrize(int argc, char **argv) {
    using namespace boost::program_options;
    options_description desc("Random Number Generator");
    desc.add_options()
        ("help,h", "Show help message")
        ("seed,s", value<long int>(&seed)->default_value(seed), "Random seed")
        ("size,n", value<size_t>(&nsize)->default_value(nsize), "Size of output")
        ("mean,m", value<double>(&mean)->default_value(mean), "Mean of distribution")
        ("sdev,d", value<double>(&sdev)->default_value(sdev), "Standard deviation of distribution")
        ("output,o", value<std::string>(&filename)->default_value(filename), "Output file")
        ("uniform", bool_switch(&Unif), "Uniform distribution")
        ("normal", bool_switch(&Norm), "Normal distribution")
        ("poisson", bool_switch(&Pois), "Poisson distribution");
    
    variables_map vm;
    store(parse_command_line(argc, argv, desc), vm);
    notify(vm);
    if (vm.count("help")) {
        std::cout << desc << "\n";
        exit(0);
    }
    /*******
A faire ici
si seed == 0 => prendre une valeur d'un "random device"
puis initialiser le générateur avec la seed
    ********/
}

int main(int argc, char **argv) {
    parametrize(argc, argv);
    /****************
1. ouvrir le fichier
2. choisir la bonne distribution (avec les paramètres)
3. ecrire "nsize" nombres dans le fichier
     ****************/
    return 0;
}
