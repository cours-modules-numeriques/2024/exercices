#include "main.h"

bool Unif, Norm, Pois;
size_t nsize = 1;
double mean = 0, sdev = 1;
std::string filename("output.txt");
long int seed = 0;
std::mt19937 rng;
std::ofstream outf;

void parametrize(int argc, char **argv) {
    using namespace boost::program_options;
    options_description desc("Random Number Generator");
    desc.add_options()
        ("help,h", "Show help message")
        ("seed,s", value<long int>(&seed)->default_value(seed), "Random seed")
        ("size,n", value<size_t>(&nsize)->default_value(nsize), "Size of output")
        ("mean,m", value<double>(&mean)->default_value(mean), "Mean of distribution")
        ("sdev,d", value<double>(&sdev)->default_value(sdev), "Standard deviation of distribution")
        ("output,o", value<std::string>(&filename)->default_value(filename), "Output file")
        ("uniform", bool_switch(&Unif), "Uniform distribution")
        ("normal", bool_switch(&Norm), "Normal distribution")
        ("poisson", bool_switch(&Pois), "Poisson distribution");
    
    variables_map vm;
    store(parse_command_line(argc, argv, desc), vm);
    notify(vm);
    if (vm.count("help")) {
        std::cout << desc << "\n";
        exit(0);
    }
    std::random_device rd;
    if (seed == 0) seed = rd();
    rng.seed(seed);
}

int main(int argc, char **argv) {
    parametrize(argc, argv);
    outf.open(filename);
    if (Unif) {
        double s3 = std::sqrt(3)*sdev;
        std::uniform_real_distribution<> dist(mean-s3, mean+s3);
        for (size_t n=0; n<nsize; n++) outf << dist(rng) << " ";
    } else if (Norm) {
        std::normal_distribution<> dist(mean, sdev);
        for (size_t n=0; n<nsize; n++) outf << dist(rng) << " ";
    } else {
        std::poisson_distribution<> dist(mean);
        for (size_t n=0; n<nsize; n++) outf << dist(rng) << " ";
    }
    outf << std::endl;
    outf.close();
    return 0;
}
