#include "main.h"

using namespace std;
using namespace boost::program_options;
using namespace boost::numeric::odeint;

typedef complex<double> cmplx;
typedef vector<cmplx> qtype;
typedef fftw_complex ftc;

size_t nsize = 1470; // 2*3*5*7*7 = 1470, 2*3*5 = 30, 2*3*5*7 = 210, 210*30 = 6300, 210*210 = 44100
const double _atol(1e-6), _rtol(1e-4);
double dt(0.1), Tmax(5.), length(10.);
string ofname("output.txt");
fftw_plan fftfwd, fftbwd;

void dxdt(const qtype &psk, qtype &dpsk, const double /*t*/) {
}

void printout(const qtype &psik, double t) {
}

int main(int argc, char **argv) {
    fftfwd = fftw_plan_dft_1d(nsize, 0, 0, FFTW_FORWARD, FFTW_ESTIMATE);
    fftbwd = fftw_plan_dft_1d(nsize, 0, 0, FFTW_BACKWARD, FFTW_ESTIMATE);

    return 0;
}