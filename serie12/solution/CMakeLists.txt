cmake_minimum_required (VERSION 3.20)
project (schrodinger_equation)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++17")
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -W -Wall -Wextra")
set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -O3")

add_executable(Schrodinger schrodinger_solution.cc)
target_link_libraries(Schrodinger boost_program_options fftw3)
