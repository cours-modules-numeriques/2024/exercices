#include "main.h"

using namespace std;
using namespace boost::program_options;
using namespace boost::numeric::odeint;

typedef complex<double> cmplx;
typedef vector<cmplx> qtype;
typedef fftw_complex ftc;

size_t nsize = 1470; // 2*3*5*7*7 = 1470, 2*3*5 = 30, 2*3*5*7 = 210, 210*30 = 6300, 210*210 = 44100
const double _atol(1e-6), _rtol(1e-4);
double dt(0.1), Tmax(5.), length(10.), normalize;
cmplx kscale;
string pfname("params.txt"), ofname("output.txt");
ofstream outf;
fftw_plan fftfwd, fftbwd;

void parse_options(int argc, char **argv) {
    try {
        options_description desc("Network of coupled oscillators");
        desc.add_options()
            ("help,h", "Show help message")
            ("step,s", value<double>(&dt)->default_value(dt), "Time step")
            ("maxtime,t", value<double>(&Tmax)->default_value(Tmax), "End time")
            ("length,l", value<double>(&length)->default_value(length), "Space length")
            ("size,n", value<size_t>(&nsize)->default_value(nsize), "Size of system")
            ("outfile,o", value<string>(&ofname)->default_value(ofname), "Output file")
            ("parfile,p", value<string>(&pfname)->default_value(pfname), "Model parameters file");
        
        variables_map vm;
        store(parse_command_line(argc, argv, desc), vm);
        notify(vm);
        if (vm.count("help")) {
            cout << desc << "\n";
            exit(0);
        }
    } catch(exception& e) {
        cerr << e.what() << "\n";
        exit(1);
    }
}

void dxdt(const qtype &psk, qtype &dpsk, const double /*t*/) {
    size_t nmid = nsize/2;
    double kn(0.);
    for (size_t n=1; n<=nmid; n++) {
        kn++;
        dpsk[n] = -1i*(kscale*kn*kn*psk[n]);
    }
    for (size_t n=nmid+1; n<nsize; n++) {
        kn--;
        dpsk[n] = -1i*(kscale*kn*kn*psk[n]);
    }
}

void printout(const qtype &psik, double t) {
    qtype psi(psik);
    fftw_execute_dft(fftbwd, (ftc*)(psi.data()), (ftc*)(psi.data()));
    outf << t;
    for (size_t n=0; n<nsize; n++) outf << " " << psi[n].real()*normalize;
    for (size_t n=0; n<nsize; n++) outf << " " << psi[n].imag()*normalize;
    outf << endl;
}

int main(int argc, char **argv) {
    parse_options(argc, argv);
    fftfwd = fftw_plan_dft_1d(nsize, 0, 0, FFTW_FORWARD, FFTW_ESTIMATE);
    fftbwd = fftw_plan_dft_1d(nsize, 0, 0, FFTW_BACKWARD, FFTW_ESTIMATE);

    kscale = M_PI/length;
    kscale *= -2i*kscale;
    normalize = 1./(double)nsize;

    qtype psi(nsize+10, 0.);
    for (size_t nx=nsize/2; nx<2*nsize/3; nx++) 
        psi[nx] = 144.*(normalize*nx-.5)*(.666-normalize*nx);
    fftw_execute_dft(fftfwd, (ftc*)(psi.data()), (ftc*)(psi.data()));

    outf.open(ofname);
    runge_kutta_dopri5<qtype> dp5;
    auto stepper = make_dense_output(_atol, _rtol, dp5);
    int step = integrate_const(stepper, dxdt, psi, 0., Tmax, dt, printout);
    outf.close();

    outf.open(pfname);
    outf << "Steps " << step << endl
         << "Time " << Tmax << endl
         << "Tstep " << dt << endl
         << "Size " << nsize << endl
         << "Length " << length << endl;
    outf.close();

    return 0;
}