#include "main.h"

using namespace boost::numeric::odeint;
using namespace std;

typedef vector<double> datatype;

double _atol(1e-15);
double _rtol(1e-10);
double dt = 0.05;
double tmax = 4;
double lam = 1;

string outfile("qm.txt");
ofstream outf;

void dxdt(const datatype &x, datatype &dx, const double q) {
    dx[0] = x[1];
    dx[1] = (q*q-lam)*x[0];
}

void printout(const datatype &x, double t) {
    outf << t << " " << x[0] << endl;
}

int main(int argc, char **argv) {
// ---------------
    double t = 0, p0 = 1, p1 = 0;
    if (argc > 1) lam = stod(argv[1]);
    if (argc > 2) p0 = stod(argv[2]);
    if (argc > 3) p1 = stod(argv[3]);
    datatype phi{p0, p1};
    outf.open(outfile);
    outf << setprecision(12);
    runge_kutta_dopri5<datatype> dp5;
    auto stepper = make_dense_output(_atol, _rtol, dp5);
    integrate_const(stepper, dxdt, phi, t, tmax, dt, printout);
    outf.close();
// ---------------
    return 0;
}
