#include "main.h"

size_t nsize = 1;
double dt = 0.01;
double tmax = 10;
double kmin = 0.5, kmax = 5;
double pmean = 0, pvar = 1;
double lambda = 0;
std::vector<double> k;
std::string filename("output.txt");

long int seed = 0;
std::mt19937 rng;
std::ofstream outf;

void parametrize(int argc, char **argv) {
    using namespace boost::program_options;
    options_description desc("Oscillator chain");
    desc.add_options()
        ("help,h", "Show help message")
        ("seed,s", value<long int>(&seed)->default_value(seed), "Random seed")
        ("size,n", value<size_t>(&nsize)->default_value(nsize), "Size of output")
        ("lambda,l", value<double>(&lambda)->default_value(lambda), "Coupling constant")
        ("tmax,t", value<double>(&tmax)->default_value(tmax), "End time")
        ("delta,d", value<double>(&dt)->default_value(dt), "Time step")
        ("output,o", value<std::string>(&filename)->default_value(filename), "Output file");
    
    variables_map vm;
    store(parse_command_line(argc, argv, desc), vm);
    notify(vm);
    if (vm.count("help")) {
        std::cout << desc << "\n";
        exit(0);
    }
    std::random_device rd;
    if (seed == 0) seed = rd();
    rng.seed(seed);
}

void verlet_step(double &t, std::vector<double> &qp) {
    std::vector<double> cplng(nsize, 0.);
    double cg = qp[0]-qp[2*nsize-2];
    cg *= cg*cg;
    cplng[nsize-1] -= cg;
    cplng[0] += cg;
    for (size_t n=1; n<nsize; n++) {
        cg = qp[2*n]-qp[2*n-2];
        cg *= cg*cg;
        cplng[n-1] -= cg;
        cplng[n] += cg;
    }
    for (size_t n=0; n<nsize; n++) {
        qp[2*n+1] -= (k[n]*qp[2*n]+lambda*cplng[n])*dt;
        qp[2*n] += qp[2*n+1]*dt;
    }
    t += dt;
}

int main(int argc, char **argv) {
    parametrize(argc, argv);
    k.resize(nsize);
    std::uniform_real_distribution<> dunif(kmin, kmax);
    for (size_t n=0; n<nsize; n++) k[n] = dunif(rng);
    std::vector<double> qp(2*nsize);
    std::normal_distribution<> dnorm(pmean, pvar);
    for (size_t n=0; n<nsize; n++) qp[2*n] = dnorm(rng);
    double t = 0;
    outf.open(filename);
    outf << lambda;
    for (auto I : k) outf << " " << I << " " << 0;
    outf << std::endl << t;
    for (auto I : qp) outf << " " << I;
    outf << std::endl;
    while (t <= tmax-dt) {
        verlet_step(t, qp);
        outf << t;
        for (auto I : qp) outf << " " << I;
        outf << std::endl;
    }
    outf.close();
    return 0;
}
