#include "main.h"

std::vector<double> k;
std::string filename("output.txt");

long int seed = 0;
std::mt19937 rng;
std::ofstream outf;

void parametrize(int argc, char **argv) {
    using namespace boost::program_options;
    options_description desc("Oscillator chain");
    desc.add_options()
        ("help,h", "Show help message")
        ("seed,s", value<long int>(&seed)->default_value(seed), "Random seed")
        ("size,n", value<size_t>(&nsize)->default_value(nsize), "Size of output")
        ("lambda,l", value<double>(&lambda)->default_value(lambda), "Coupling constant")
        ("tmax,t", value<double>(&tmax)->default_value(tmax), "End time")
        ("delta,d", value<double>(&dt)->default_value(dt), "Time step")
        ("output,o", value<std::string>(&filename)->default_value(filename), "Output file");
    
    variables_map vm;
    store(parse_command_line(argc, argv, desc), vm);
    notify(vm);
    if (vm.count("help")) {
        std::cout << desc << "\n";
        exit(0);
    }
    std::random_device rd;
    if (seed == 0) seed = rd();
    rng.seed(seed);
}

void verlet_step(double &t, std::vector<double> &qp) {
    // à adapter de la série 2 avec qp={q1,p1,q2,p2,...,qN,pN}
}

int main(int argc, char **argv) {
    parametrize(argc, argv);
    // remplir le vecteur k avec des nombres aléatoires, 
    // initialiser le vecteur qp des positions et vitesses 
    // intégrer les équations de t=0 à t=tmax
    
    return 0;
}
