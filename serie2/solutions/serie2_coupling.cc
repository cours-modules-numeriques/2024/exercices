#include <iostream>
#include <string>
#include <cmath>
#include <fstream>
#include <vector>

double dt = 0.01;
std::vector<double> k{2, 2};
double lambda = 1;
double tmax = 100;
std::string filename("output.txt");

void verlet_step(double &t, std::vector<double> &qp) {
    size_t nsize = k.size();
    double cplng = lambda*(qp[2]-qp[0]);
    qp[1] -= (k[0]*qp[0]-cplng)*dt;
    qp[3] -= (k[1]*qp[2]+cplng)*dt;
    qp[0] += qp[1]*dt;
    qp[2] += qp[3]*dt;
    t += dt;
}

void euler_step(double &t, std::vector<double> &qp) {
    size_t nsize = k.size();
    double cplng = lambda*(qp[2]-qp[0]);
    double p1 = qp[1], p2 = qp[3];
    qp[1] -= (k[0]*qp[0]-cplng)*dt;
    qp[3] -= (k[1]*qp[2]+cplng)*dt;
    qp[0] += p1*dt;
    qp[2] += p2*dt;
    t += dt;
}

int main(int argc, char **argv) {
    if (argc > 3) {
        k = {std::stod(argv[1]), std::stod(argv[2])};
        lambda = std::stod(argv[3]);
    }
    if (argc > 4) filename = argv[4];
    std::ofstream outfile(filename);
    double t = 0;
    std::vector<double> qp{0, 1, 0, -1};
    while (t <= tmax) {
        outfile << t;
        for (size_t n = 0; n < qp.size(); n++)
            outfile << " " << qp[n];
        outfile << std::endl;
 //        euler_step(t, qp);
        verlet_step(t, qp);
    }
    outfile.close();
    return 0;
}
