#include "main.h"

using namespace boost::numeric::odeint;
using namespace std;

typedef vector<double> datatype;

double _atol(1e-15);
double _rtol(1e-10);
double dt = 0.1;
double h;
double tmax = 30;

string outfile("pendule");
ofstream outf;

/* Forme générale de l'équation différentielle: dx/dt = F(x, t). 
   Ici on calcule F(x, t). */

void dxdt(const datatype &x, datatype &dx, const double t) {
}

/* Cette fonction est appellée chaque fois que t=n dt.
   On sauve la solution dans un fichier. */

void printout(const datatype &x, double t) {
}

/* -------  Coder les autres méthodes ------- */
void euler_step(double &t, datatype &x) {
    /***** 1 pas de euler explicite ****/
}

/*** etc. ***/

int main(int, char**) {
// ---------------
    double t = 0, q0 = 0, p0 = 1.98;
    datatype qp{q0, p0};
    outf.open(outfile+"_dp5.txt");
    /***** 
        par défaut les nombres sont représentés avec 6 chiffres, 
        comme notre tolérence est 1e-15, il nous faut plus de précision à l'affichage
    *****/
    outf << setprecision(16);
   
    /***** 
        Méthode BOOST odeint: 
           1. on choisit le schéma numérique: DOPRI5
           2. on choisit les paramètres de tolérence et l'output dense: stepper
           3. on lance l'intégration: t=0...tmax, x(0)=qp, x'(t)=dxdt(x,t),
                "printout" executé pour chaque t=n*dt
    *****/
    runge_kutta_dopri5<datatype> dp5;
    auto stepper = make_dense_output(_atol, _rtol, dp5);
    integrate_const(stepper, dxdt, qp, t, tmax, dt, printout);
    outf.close();
// ---------------
    /***** 
        faire la même chose avec les autres méthodes, pour chaque valeur de h choisie
    *****/
    return 0;
}
