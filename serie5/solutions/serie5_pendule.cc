#include "main.h"

using namespace boost::numeric::odeint;
using namespace std;

typedef vector<double> datatype;

double _atol(1e-15);
double _rtol(1e-10);
double dt = 0.1;
double h;
double tmax = 30;

string outfile("pendule");
ofstream outf;

void dxdt(const datatype &x, datatype &dx, const double t) {
    dx[0] = x[1];
    dx[1] = -sin(x[0]);
}

void printout(const datatype &x, double t) {
    outf << t;
    for (auto I : x) outf << " " << I;
    outf << endl;
}

void verlet_step(double &t, datatype &x) {
    x[1] -= sin(x[0])*h;
    x[0] += x[1]*h;
    t += h;
}

void verlet2_step(double &t, datatype &x) {
    double p12 = x[1]-0.5*h*sin(x[0]);
    x[0] += p12*h;
    x[1] = p12-0.5*h*sin(x[0]);
    t += h;
}

void euler_step(double &t, vector<double> &x) {
    double p1 = x[1];
    x[1] -= sin(x[0])*h;
    x[0] += p1*h;
    t += h;
}

void euler_implicit_step(double &t, datatype &x) {
    x[1] = (x[1]-h*sin(x[0]))/(1+h*h*cos(x[0]));
    x[0] += x[1]*h;
    t += h;
}

void euler2_step(double &t, vector<double> &x) {
    double p1 = x[1], q1 = x[0];
    x[1] -= (sin(q1)+0.5*h*p1*cos(q1))*h;
    x[0] += (p1-0.5*h*sin(q1))*h;
    t += h;
}

int main(int, char**) {
// ---------------
    double t = 0, q0 = 0, p0 = 1.98;
    datatype qp{q0, p0};
    outf.open(outfile+"_dp5.txt");
    outf << setprecision(16);
    runge_kutta_dopri5<datatype> dp5;
    auto stepper = make_dense_output(_atol, _rtol, dp5);
    integrate_const(stepper, dxdt, qp, t, tmax+dt*.9, dt, printout);
    outf.close();
// ---------------
    vector<string> methods{"e2", "e1", "ei", "sv", "s2"};
    h = 0.5;
    for (size_t hn=0; hn<6; hn++) {
        h *= 0.2;
        cout << h << " ";
        for (size_t mn=0; mn<methods.size(); mn++) {
            qp[0] = q0;
            qp[1] = p0;
            t = 0;
            double next_t = 0;
            outf.open(outfile+"_"+to_string(hn)+"_"+methods[mn]+".txt");
            outf << setprecision(16);
            while (t<tmax-dt*0.1) {
                switch (mn) {
                case 0:
                    while (t<next_t) euler2_step(t, qp);
                    break;
                case 1:
                    while (t<next_t) euler_step(t, qp);
                    break;
                case 2:
                    while (t<next_t) euler_implicit_step(t, qp);
                    break;
                case 3:
                    while (t<next_t) verlet_step(t, qp);
                    break;
                case 4:
                    while (t<next_t) verlet2_step(t, qp);
                    break;
                }
                printout(qp, next_t);
                next_t += dt;
            }
            outf.close();
        }
    }
    cout << endl;
    return 0;
}
