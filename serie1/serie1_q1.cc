#include <iostream>
#include <string>
#include <fstream>

std::string filename("hello.txt");
int nrep = 10;

//  "increment": prend un argument "int" en entrée (k), lui ajoute 1 et ne retourne rien (void)
//  &k signifie que la fonction modifie directement la variable originale et pas une copie locale 
void increment(int &k) {
    k += 1;
}

int main(int, char**) {
    int n = 0;
    while (n<nrep) {
        increment(n);
        std::cout << "hello " << n << std::endl;
    }
    // Décommenter les lignes suivantes pour écrire dans un fichier 
    /* 
    std::ofstream outfile(filename);    
    n = 0;
    while (n<nrep) {
        increment(n);
        outfile<< "hello " << n << std::endl;
    }
    outfile.close();
    */
    return 0;
}
