#include <iostream>
#include <string>
#include <cmath>
#include <fstream>

double dt = 0.01;
double k = 2;
double tmax = 5;
std::string filename("output.txt");

void euler_step(double &t, double &q, double &p) {
    double q0 = q;
    q += p*dt;
    p -= k*q0*dt;
    t += dt;
}

int main(int, char**) {
    std::ofstream outfile(filename);
    double t = 0;
    double q = 1;
    double p = 0;
    while (t <= tmax) {
        outfile << t << " " << q << " " << p << std::endl;
        euler_step(t, q, p);
    }
    outfile.close();
    return 0;
}
